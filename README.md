net.voj_tech.cloudreach.accidents
=================================

Task
----

### Project Summary

Create a backend app that pulls down one or more public data sets, validates the format of the data (could just be a
schema validation, it doesn't need to be complex), optionally stores it into a central data store, and makes it
available for consumption via a REST API.

### Some examples and ideas

- An API that informs users whether to take an umbrella or not by pulling down and making use of weather forecast data
  for a given location
  
- An API that returns the 10 most popular taxi routes in New York

Solution
--------

I have created a tool to analyze UK traffic accidents and report local authorities with the highest number of accidents.
The dataset I have chosen is here: https://www.kaggle.com/daveianhickey/2000-16-traffic-flow-england-scotland-wales/data

There are 3 projects created in Eclipse using Java and Maven:

1. data - This is a data abstraction layer which saves, loads and queries the downloaded and crunched data.
   - The back end is actually implemented with Elasticsearch as I wanted to try it out and experiment with it. In reality
     Elasticsearch might not be the best tool to use for data persistence.
   - Apart from Elasticsearch it references com.google.code.gson to work with JSON.

2. data-scraper - This is a tool to download all the data from Kaggle and store them in the Elasticsearch cluster.
   - References the data project and also com.google.code.gson to work with JSON and org.apache.commons to parse CSV
     formatted data.

3. server - This is a very basic REST server which queries the Elasticsearch back end and provides the resulting data.
   - Elasticsearch already provides a REST API to query the data but I wasn't sure whether that was enough for the task.
   - It is written using the Spring Framework and it also references the data project.

Usage
-----

### data-scraper

This is a command line tool and it requires one command line argument which is a path to a text file with Kaggle cookies
required for authentication.

To obtain the file:

1. Login to Kaggle in Chrome.
2. Export the cookies using the "cookie.txt export" extension and save them in a file.

It is ok to forcibly stop the tool once it's finished downloading the local authority definitions and is in the middle
of downloading the traffic accident files. The REST API would then work with a subset of the data for demonstration
purposes. The accident files are very large and it would take long to download them completely.

There is an official command-line tool for the Kaggle platform but it doesn't seem to support printing the downloaded
files to standard output for an external tool to pick them up this way. It seems the only way to use the tool is to
download complete files to the file system.

### server

1. Simply run the project/built JAR as any other Spring Framework based project.
2. Access http://localhost:8080/local-authority to see the 10 local authorities with the most traffic accidents.
