package net.voj_tech.cloudreach.accidents.server;

import java.net.UnknownHostException;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.voj_tech.cloudreach.accidents.data.LocalAuthority;

/**
 * Controller for accessing local authorities.
 * 
 * @author voj-tech
 */
@RestController
@RequestMapping("/local-authority")
public class LocalAuthorityController extends Controller {

	private static final String DEFAULT_NUM_LOCAL_AUTHORITIES_LIMIT = "10";

	public LocalAuthorityController() throws UnknownHostException {
		super();
	}

	/**
	 * Returns a local authority specified by code.
	 * 
	 * @param code
	 *            The local authority code.
	 * @return Local authority specified by code.
	 */
	@GetMapping("/{code}")
	public LocalAuthority readLocalAuthority(@PathVariable String code) {
		return dataMapper.loadLocalAuthorityByCode(code);
	}

	/**
	 * Returns local authorities ordered by the number of accidents.
	 * <p>
	 * The actual numbers of accidents are not returned as this is not part of the
	 * LocalAuthority class but it would be easy to include them since we get them
	 * as part of the DataMapper results.
	 * 
	 * @param numLocalAuthoritiesLimit
	 *            Limit of the number of local authorities returned.
	 * @return Local authorities ordered by the number of accidents.
	 */
	@GetMapping
	public Collection<LocalAuthority> readLocalAuthorities(
			@RequestParam(value = "limit", defaultValue = DEFAULT_NUM_LOCAL_AUTHORITIES_LIMIT) int numLocalAuthoritiesLimit) {
		return dataMapper
				.loadLocalAuthoritiesByCodes(dataMapper.loadLocalAuthorityAccidentCounts(numLocalAuthoritiesLimit)
						.stream().map(laac -> laac.getLocalAuthorityCode()).collect(Collectors.toList()));
	}

}
