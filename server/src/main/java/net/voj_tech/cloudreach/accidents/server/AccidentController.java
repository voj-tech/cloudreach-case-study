package net.voj_tech.cloudreach.accidents.server;

import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.voj_tech.cloudreach.accidents.data.Accident;

/**
 * Controller for accessing accidents.
 * 
 * @author voj-tech
 */
@RestController
@RequestMapping("/accident")
public class AccidentController extends Controller {

	public AccidentController() throws UnknownHostException {
		super();
	}

	/**
	 * Returns an accident specified by id.
	 * 
	 * @param id
	 *            The accident id.
	 * @return Accident specified by id.
	 */
	@GetMapping("/{id}")
	public Accident readAccident(@PathVariable String id) {
		return dataMapper.loadAccidentById(id);
	}

}
