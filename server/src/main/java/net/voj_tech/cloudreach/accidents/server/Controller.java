package net.voj_tech.cloudreach.accidents.server;

import java.net.UnknownHostException;

import net.voj_tech.cloudreach.accidents.data.DataMapper;
import net.voj_tech.cloudreach.accidents.data.ElasticsearchDataMapper;

/**
 * Base controller for the server which declares and initialises our
 * Elasticsearch data mapper.
 * 
 * @author voj-tech
 */
abstract class Controller {

	/**
	 * An Elasticsearch host name.
	 * <p>
	 * This would be in an external configuration file.
	 */
	private static final String ELASTICSEARCH_HOST_NAME = "localhost";

	/**
	 * An Elasticsearch port.
	 * <p>
	 * This would be in an external configuration file.
	 */
	private static final int ELASTICSEARCH_PORT = 9300;

	/**
	 * The Elasticsearch cluster name.
	 * <p>
	 * This would be in an external configuration file.
	 */
	private static final String ELASTICSEARCH_CLUSTER_NAME = "cloudreach-traffic-accidents";

	protected DataMapper dataMapper;

	public Controller() throws UnknownHostException {
		dataMapper = new ElasticsearchDataMapper(ELASTICSEARCH_HOST_NAME, ELASTICSEARCH_PORT,
				ELASTICSEARCH_CLUSTER_NAME);
	}

}
