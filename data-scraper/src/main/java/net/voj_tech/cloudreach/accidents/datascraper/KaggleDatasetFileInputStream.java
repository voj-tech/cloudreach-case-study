package net.voj_tech.cloudreach.accidents.datascraper;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.function.Supplier;

/**
 * Connects to the Kaggle platform and downloads a dataset file.
 * 
 * @author voj-tech
 */
class KaggleDatasetFileInputStream extends InputStream {

	private static final String URL_FORMAT = "https://www.kaggle.com/%s/downloads/%s";

	private HttpURLConnection httpURLConnection;
	private InputStream stream;

	/**
	 * Constructs an instance.
	 * 
	 * @param cookieSupplier
	 *            A supplier of the Kaggle cookies required for a successful
	 *            authentication.
	 * @param datasetName
	 *            The name of the dataset.
	 * @param fileName
	 *            The name of the file within the dataset.
	 * @throws IOException
	 */
	public KaggleDatasetFileInputStream(Supplier<String> cookieSupplier, String datasetName, String fileName)
			throws IOException {
		URL url = new URL(String.format(URL_FORMAT, datasetName, fileName));
		httpURLConnection = (HttpURLConnection) url.openConnection();
		httpURLConnection.setRequestProperty("Cookie", cookieSupplier.get());
		stream = httpURLConnection.getInputStream();
	}

	@Override
	public int available() throws IOException {
		return stream.available();
	}

	@Override
	public void close() throws IOException {
		stream.close();
		httpURLConnection.disconnect();
	}

	@Override
	public void mark(int readlimit) {
		stream.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return stream.markSupported();
	}

	@Override
	public int read() throws IOException {
		return stream.read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return stream.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return stream.read(b, off, len);
	}

	@Override
	public void reset() throws IOException {
		stream.reset();
	}

	@Override
	public long skip(long n) throws IOException {
		return stream.skip(n);
	}

}
