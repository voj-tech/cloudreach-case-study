package net.voj_tech.cloudreach.accidents.datascraper.datasetfileparser;

import java.io.Closeable;

/**
 * Parses a file and emits entities.
 * 
 * @author voj-tech
 *
 * @param <T>
 *            The type of the recognised entity.
 */
public interface DatasetFileParser<T> extends Iterable<T>, Closeable {
}
