package net.voj_tech.cloudreach.accidents.datascraper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.zip.ZipInputStream;

import net.voj_tech.cloudreach.accidents.data.Accident;
import net.voj_tech.cloudreach.accidents.data.DataMapper;
import net.voj_tech.cloudreach.accidents.data.ElasticsearchDataMapper;
import net.voj_tech.cloudreach.accidents.data.LocalAuthority;
import net.voj_tech.cloudreach.accidents.datascraper.datasetfileparser.CSVDatasetFileParser;
import net.voj_tech.cloudreach.accidents.datascraper.datasetfileparser.DatasetFileParser;
import net.voj_tech.cloudreach.accidents.datascraper.datasetfileparser.GeoJSONDatasetFileParser;

/**
 * Scrapes data from the Kaggle platform storing them in an Elasticsearch back
 * end.
 * <p>
 * Four files are downloaded from the
 * "daveianhickey/2000-16-traffic-flow-england-scotland-wales" dataset;
 * "Local_Authority_Districts_Dec_2016.geojson", "accidents_2005_to_2007.csv",
 * "accidents_2009_to_2011.csv" and "accidents_2012_to_2014.csv". The first one
 * is a definition of local authorities and the others are traffic accident
 * records with the information of the local authority where each accident
 * happened.
 * 
 * @author voj-tech
 */
public class DataScraper {

	private static final String KAGGLE_DATASET_NAME = "daveianhickey/2000-16-traffic-flow-england-scotland-wales";

	/**
	 * An Elasticsearch host name.
	 * <p>
	 * This would be in an external configuration file.
	 */
	private static final String ELASTICSEARCH_HOST_NAME = "localhost";

	/**
	 * An Elasticsearch port.
	 * <p>
	 * This would be in an external configuration file.
	 */
	private static final int ELASTICSEARCH_PORT = 9300;

	/**
	 * The Elasticsearch cluster name.
	 * <p>
	 * This would be in an external configuration file.
	 */
	private static final String ELASTICSEARCH_CLUSTER_NAME = "cloudreach-traffic-accidents";

	/**
	 * Initialises Elasticsearch indexes, downloads and parses the dataset files
	 * storing the required data in the Elasticsearch cluster.
	 * 
	 * @param args
	 *            One element is required and it is a path to a Wget compatible text
	 *            file with Kaggle cookies.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		if (args.length < 1) {
			throw new RuntimeException("Cookies file name not specified.");
		}

		Supplier<String> cookieSupplier = new FileCookieSupplier(args[0]);

		try (ElasticsearchDataMapper dataMapper = new ElasticsearchDataMapper(ELASTICSEARCH_HOST_NAME,
				ELASTICSEARCH_PORT, ELASTICSEARCH_CLUSTER_NAME)) {
			dataMapper.ensureIndicesInitialised();

			scrapeLocalAuthorityData(dataMapper, cookieSupplier,
					Arrays.asList("Local_Authority_Districts_Dec_2016.geojson"));

			scrapeAccidentData(dataMapper, cookieSupplier, Arrays.asList("accidents_2005_to_2007.csv",
					"accidents_2009_to_2011.csv", "accidents_2012_to_2014.csv"));
		}
	}

	/**
	 * A function which throws IOException.
	 * <p>
	 * Makes Java compiler happy.
	 * 
	 * @author voj-tech
	 *
	 * @param <T>
	 *            The type of the input to the function.
	 * @param <R>
	 *            The type of the result of the function.
	 */
	@FunctionalInterface
	interface CheckedFunction<T, R> {
		R apply(T t) throws IOException;
	}

	/**
	 * Scrapes local authority data.
	 * 
	 * @param dataMapper
	 *            A data mapper.
	 * @param cookieSupplier
	 *            A supplier of the Kaggle cookies.
	 * @param fileNames
	 *            Specify files to download.
	 * @throws IOException
	 */
	private static void scrapeLocalAuthorityData(DataMapper dataMapper, Supplier<String> cookieSupplier,
			Iterable<String> fileNames) throws IOException {
		Map<String, BiConsumer<LocalAuthority, Object>> dataMappings = new HashMap<String, BiConsumer<LocalAuthority, Object>>();
		dataMappings.put("lad16cd", (localAuthority, code) -> localAuthority.setCode(code.toString()));
		dataMappings.put("lad16nm", (localAuthority, name) -> localAuthority.setName(name.toString()));

		scrapeEntityData(
				cookieSupplier, fileNames, inputStream -> new GeoJSONDatasetFileParser<LocalAuthority>(inputStream,
						LocalAuthority.class, dataMappings),
				localAuthority -> dataMapper.storeLocalAuthority(localAuthority));
	}

	/**
	 * Scrapes accident data.
	 * 
	 * @param dataMapper
	 *            A data mapper.
	 * @param cookieSupplier
	 *            A supplier of the Kaggle cookies.
	 * @param fileNames
	 *            Specify files to download.
	 * @throws IOException
	 */
	private static void scrapeAccidentData(DataMapper dataMapper, Supplier<String> cookieSupplier,
			Iterable<String> fileNames) throws IOException {
		Map<String, BiConsumer<Accident, Object>> dataMappings = new HashMap<String, BiConsumer<Accident, Object>>();
		// The original idea was to use the Accident_Index column as the source of
		// accident ids but it turns out the data in the column is broken. Locally
		// generated ids are assigned later.
		dataMappings.put("Local_Authority_(Highway)",
				(accident, localAuthorityCode) -> accident.setLocalAuthorityCode(localAuthorityCode.toString()));

		scrapeEntityData(cookieSupplier, fileNames,
				inputStream -> new CSVDatasetFileParser<Accident>(inputStream, Accident.class, dataMappings),
				accident -> dataMapper.storeAccident(accident));
	}

	/**
	 * Scrapes data.
	 * 
	 * @param cookieSupplier
	 *            A supplier of the Kaggle cookies.
	 * @param fileNames
	 *            Specify files to download.
	 * @param createDatasetFileParser
	 *            A dataset file parser to use.
	 * @param storeEntity
	 *            A callback to store each entity.
	 * @throws IOException
	 */
	private static <T> void scrapeEntityData(Supplier<String> cookieSupplier, Iterable<String> fileNames,
			CheckedFunction<InputStream, DatasetFileParser<T>> createDatasetFileParser, Consumer<T> storeEntity)
			throws IOException {
		for (String fileName : fileNames) {
			System.out.print("Scrapping " + fileName);

			// We need to unzip the stream so we wrap the Kaggle dataset file input stream
			// with ZipInputStream and get the next entry as there is a file in the ZIP
			// file.
			try (InputStream inputStream = new KaggleDatasetFileInputStream(cookieSupplier, KAGGLE_DATASET_NAME,
					fileName); ZipInputStream zipInputStream = new ZipInputStream(inputStream)) {
				zipInputStream.getNextEntry();

				try (DatasetFileParser<T> datasetFileParser = createDatasetFileParser.apply(zipInputStream)) {
					for (T entity : datasetFileParser) {
						storeEntity.accept(entity);
						System.out.print('.');
					}
				}
			}

			System.out.println();
		}
	}

}
