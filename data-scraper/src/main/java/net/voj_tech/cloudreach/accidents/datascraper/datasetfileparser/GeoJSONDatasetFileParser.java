package net.voj_tech.cloudreach.accidents.datascraper.datasetfileparser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

/**
 * Parses a specific GeoJSON file and emits entities.
 * <p>
 * Assumes the type of the parsed file is "FeatureCollection".
 * 
 * @author voj-tech
 *
 * @param <T>
 *            The type of the recognised entity.
 */
public class GeoJSONDatasetFileParser<T> implements DatasetFileParser<T> {

	private JsonReader reader;
	private Class<T> entityClass;
	private Map<String, BiConsumer<T, Object>> dataMappings;

	/**
	 * Constructs an instance.
	 * 
	 * @param inputStream
	 *            Input stream of the file.
	 * @param entityClass
	 *            The class of the recognised entity.
	 * @param dataMappings
	 *            Mappings specifying how to initialise entities using the parsed
	 *            data.
	 * @throws IOException
	 */
	public GeoJSONDatasetFileParser(InputStream inputStream, Class<T> entityClass,
			Map<String, BiConsumer<T, Object>> dataMappings) throws IOException {
		reader = new JsonReader(new InputStreamReader(inputStream));
		this.entityClass = entityClass;
		this.dataMappings = dataMappings;

		seekToProperty("features");
		reader.beginArray();
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				try {
					return reader.hasNext();
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
			}

			@Override
			public T next() {
				try {
					seekToProperty("properties");

					reader.beginObject();
					T entity = entityClass.newInstance();
					while (reader.peek() == JsonToken.NAME) {
						BiConsumer<T, Object> action = dataMappings.get(reader.nextName());
						if (action != null) {
							action.accept(entity, reader.nextString());
						} else {
							reader.skipValue();
						}
					}
					reader.endObject();

					// skip the rest of the "feature" object
					while (reader.peek() == JsonToken.NAME) {
						reader.nextName();
						reader.skipValue();
					}
					reader.endObject();

					return entity;
				} catch (IOException | InstantiationException | IllegalAccessException e) {
					throw new RuntimeException(e);
				}
			}

		};
	}

	@Override
	public void close() throws IOException {
		reader.close();
	}

	/**
	 * Begins an object and skips all properties until a specified property is
	 * encountered.
	 * 
	 * @param name
	 *            The property name.
	 * @throws IOException
	 */
	private void seekToProperty(String name) throws IOException {
		reader.beginObject();
		while (!name.equals(reader.nextName())) {
			reader.skipValue();
		}
	}

}
