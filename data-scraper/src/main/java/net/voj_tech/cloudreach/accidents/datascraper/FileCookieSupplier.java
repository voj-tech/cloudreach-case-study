package net.voj_tech.cloudreach.accidents.datascraper;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Reads a Wget compatible cookies file and supplies the cookies in the form of
 * the Cookie HTTP header value.
 * 
 * @author voj-tech
 */
class FileCookieSupplier implements Supplier<String> {

	private String fileName;

	public FileCookieSupplier(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String get() {
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			return stream.map(String::trim).filter(line -> !line.isEmpty() && line.charAt(0) != '#').map(line -> {
				String[] values = line.split("\t");
				if (values.length < 7) {
					throw new RuntimeException("Too few values on a line in cookies file.");
				}

				return values[5] + "=" + values[6];
			}).collect(Collectors.joining("; "));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
