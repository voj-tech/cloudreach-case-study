package net.voj_tech.cloudreach.accidents.datascraper.datasetfileparser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;
import java.util.function.BiConsumer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

/**
 * Parses a CSV file and emits entities.
 * 
 * @author voj-tech
 *
 * @param <T>
 *            The type of the recognised entity.
 */
public class CSVDatasetFileParser<T> implements DatasetFileParser<T> {

	private CSVParser csvParser;
	private Class<T> entityClass;
	private Map<String, BiConsumer<T, Object>> dataMappings;

	/**
	 * Constructs an instance.
	 * 
	 * @param inputStream
	 *            Input stream of the file.
	 * @param entityClass
	 *            The class of the recognised entity.
	 * @param dataMappings
	 *            Mappings specifying how to initialise entities using the parsed
	 *            data.
	 * @throws IOException
	 */
	public CSVDatasetFileParser(InputStream inputStream, Class<T> entityClass,
			Map<String, BiConsumer<T, Object>> dataMappings) throws IOException {
		csvParser = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(new InputStreamReader(inputStream));
		this.entityClass = entityClass;
		this.dataMappings = dataMappings;
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {

			@Override
			public boolean hasNext() {
				return csvParser.iterator().hasNext();
			}

			@Override
			public T next() {
				CSVRecord csvRecord = csvParser.iterator().next();
				T entity;
				try {
					entity = entityClass.newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					throw new RuntimeException(e);
				}
				for (Map.Entry<String, BiConsumer<T, Object>> dataMapping : dataMappings.entrySet()) {
					dataMapping.getValue().accept(entity, csvRecord.get(dataMapping.getKey()));
				}
				return entity;
			}

		};
	}

	@Override
	public void close() throws IOException {
		csvParser.close();
	}

}
