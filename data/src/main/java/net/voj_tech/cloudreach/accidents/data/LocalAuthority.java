package net.voj_tech.cloudreach.accidents.data;

/**
 * Represents a local authority.
 * 
 * @author voj-tech
 */
public class LocalAuthority {

	private String code;
	private String name;

	public LocalAuthority() {
	}

	public LocalAuthority(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
