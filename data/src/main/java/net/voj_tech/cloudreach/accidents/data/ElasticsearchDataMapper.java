package net.voj_tech.cloudreach.accidents.data;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequestBuilder;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Stores accidents and local authorities in Elasticsearch cluster and
 * subsequently allows to query the store.
 * 
 * @author voj-tech
 */
public class ElasticsearchDataMapper implements DataMapper {

	private static final String LOCAL_AUTHORITIES_INDEX_NAME = "local-authorities";
	private static final String LOCAL_AUTHORITIES_TYPE_NAME = "local_authorities";

	private static final String ACCIDENTS_INDEX_NAME = "accidents";
	private static final String ACCIDENTS_TYPE_NAME = "accidents";

	private static final String ACCIDENT_LOCAL_AUTHORITY_CODE_FIELD_NAME = "localAuthorityCode";

	private TransportClient client;
	private Gson gson;

	/**
	 * Constructs an instance.
	 * 
	 * @param hostName
	 *            An Elasticsearch host name.
	 * @param port
	 *            An Elasticsearch port.
	 * @param clusterName
	 *            The Elasticsearch cluster name.
	 * @throws UnknownHostException
	 */
	public ElasticsearchDataMapper(String hostName, int port, String clusterName) throws UnknownHostException {
		Settings settings = Settings.builder().put("cluster.name", clusterName).build();
		client = new PreBuiltTransportClient(settings);
		client.addTransportAddress(new TransportAddress(InetAddress.getByName(hostName), port));
		gson = new GsonBuilder().create();
	}

	/**
	 * Ensures indices are created before storing any data.
	 * <p>
	 * Puts a mapping such that the local authority code field is of type "keyword"
	 * allowing to search on it.
	 * 
	 * @throws IOException
	 */
	public void ensureIndicesInitialised() throws IOException {
		ensureIndicesExist();
		putAccidentsMapping();
	}

	private void ensureIndicesExist() {
		String[] existingIndices = client.admin().indices().getIndex(new GetIndexRequest()).actionGet().getIndices();
		List<String> existingIndexList = Arrays.asList(existingIndices);
		ensureIndexExists(existingIndexList, LOCAL_AUTHORITIES_INDEX_NAME);
		ensureIndexExists(existingIndexList, ACCIDENTS_INDEX_NAME);
	}

	private void ensureIndexExists(List<String> existingIndexList, String name) {
		IndicesAdminClient indicesAdminClinet = client.admin().indices();
		if (!existingIndexList.contains(name)) {
			indicesAdminClinet.prepareCreate(name).get();
		}
	}

	private void putAccidentsMapping() throws IOException {
		XContentBuilder builder = XContentFactory.jsonBuilder();
		builder.startObject();
		{
			builder.startObject("properties");
			{
				builder.startObject(ACCIDENT_LOCAL_AUTHORITY_CODE_FIELD_NAME);
				{
					builder.field("type", "keyword");
				}
				builder.endObject();
			}
			builder.endObject();
		}
		builder.endObject();

		PutMappingRequest request = new PutMappingRequest(ACCIDENTS_INDEX_NAME).type(ACCIDENTS_TYPE_NAME);
		request.source(builder);
		client.admin().indices().putMapping(request).actionGet();
	}

	@Override
	public void storeLocalAuthority(LocalAuthority localAuthority) {
		String code = localAuthority.getCode();
		String json = gson.toJson(localAuthority);
		client.prepareIndex(LOCAL_AUTHORITIES_INDEX_NAME, LOCAL_AUTHORITIES_TYPE_NAME, code)
				.setSource(json, XContentType.JSON).get();
	}

	/**
	 * {@inheritDoc} The id in the object is ignored and when the accident is stored
	 * an internal id is generated which is set on the object.
	 */
	@Override
	public void storeAccident(Accident accident) {
		String json = gson.toJson(accident);
		IndexResponse response = client.prepareIndex(ACCIDENTS_INDEX_NAME, ACCIDENTS_TYPE_NAME)
				.setSource(json, XContentType.JSON).get();
		accident.setId(response.getId());
	}

	@Override
	public LocalAuthority loadLocalAuthorityByCode(String code) {
		GetResponse response = client.prepareGet(LOCAL_AUTHORITIES_INDEX_NAME, LOCAL_AUTHORITIES_TYPE_NAME, code).get();
		return gson.fromJson(response.getSourceAsString(), LocalAuthority.class);
	}

	/**
	 * {@inheritDoc}}
	 * 
	 * @see #storeAccident
	 */
	@Override
	public Accident loadAccidentById(String id) {
		GetResponse response = client.prepareGet(ACCIDENTS_INDEX_NAME, ACCIDENTS_TYPE_NAME, id).get();
		return createAccidentFromJSONAndSetId(response.getSourceAsString(), response.getId());
	}

	@Override
	public ArrayList<LocalAuthority> loadLocalAuthoritiesByCodes(Iterable<String> codes) {
		MultiGetRequestBuilder requestBuilder = client.prepareMultiGet();
		for (String code : codes) {
			requestBuilder.add(LOCAL_AUTHORITIES_INDEX_NAME, LOCAL_AUTHORITIES_TYPE_NAME, code);
		}

		ArrayList<LocalAuthority> localAuthorities = new ArrayList<LocalAuthority>();
		for (MultiGetItemResponse itemResponse : requestBuilder.get()) {
			GetResponse response = itemResponse.getResponse();
			if (response.isExists()) {
				localAuthorities.add(gson.fromJson(response.getSourceAsString(), LocalAuthority.class));
			}
		}

		return localAuthorities;
	}

	/**
	 * {@inheritDoc}}
	 * 
	 * @see #storeAccident
	 */
	@Override
	public ArrayList<Accident> loadAccidentsByIds(Iterable<String> ids) {
		MultiGetRequestBuilder requestBuilder = client.prepareMultiGet();
		for (String id : ids) {
			requestBuilder.add(ACCIDENTS_INDEX_NAME, ACCIDENTS_TYPE_NAME, id);
		}

		ArrayList<Accident> accidents = new ArrayList<Accident>();
		for (MultiGetItemResponse itemResponse : requestBuilder.get()) {
			GetResponse response = itemResponse.getResponse();
			if (response.isExists()) {
				accidents.add(createAccidentFromJSONAndSetId(response.getSourceAsString(), response.getId()));
			}
		}

		return accidents;
	}

	@Override
	public ArrayList<LocalAuthorityAccidentCount> loadLocalAuthorityAccidentCounts(int numLocalAuthoritiesLimit) {
		ArrayList<LocalAuthorityAccidentCount> result = new ArrayList<LocalAuthorityAccidentCount>();

		String aggregationName = "by_local_authority_code";

		boolean ascOrder = false;
		SearchResponse response = client.prepareSearch()
				.addAggregation(
						AggregationBuilders.terms(aggregationName).field(ACCIDENT_LOCAL_AUTHORITY_CODE_FIELD_NAME)
								.size(numLocalAuthoritiesLimit).order(BucketOrder.count(ascOrder)))
				.execute().actionGet();

		Terms aggregation = response.getAggregations().get(aggregationName);
		for (Bucket bucket : aggregation.getBuckets()) {
			result.add(new LocalAuthorityAccidentCount(bucket.getKeyAsString(), bucket.getDocCount()));
		}

		return result;
	}

	@Override
	public void close() throws IOException {
		client.close();
	}

	/**
	 * To allow omitting accident ids when storing accidents we use the
	 * Elasticsearch document ids as accident ids.
	 * 
	 * @param json
	 *            The document source.
	 * @param id
	 *            The document id.
	 * @return The resulting accident.
	 */
	private Accident createAccidentFromJSONAndSetId(String json, String id) {
		Accident accident = gson.fromJson(json, Accident.class);
		accident.setId(id);
		return accident;
	}

}
