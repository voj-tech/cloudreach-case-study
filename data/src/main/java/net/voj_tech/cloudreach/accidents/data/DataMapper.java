package net.voj_tech.cloudreach.accidents.data;

import java.io.Closeable;
import java.util.ArrayList;

/**
 * Allows to store accidents and local authorities and subsequently query the
 * store.
 * <p>
 * An idea for a refactoring would be to split this interface into two; one for
 * accidents and the other for local authorities.
 * 
 * @author voj-tech
 */
public interface DataMapper extends Closeable {

	/**
	 * Stores a local authority.
	 * 
	 * @param localAuthority
	 *            The local authority.
	 */
	void storeLocalAuthority(LocalAuthority localAuthority);

	/**
	 * Stores an accident.
	 * 
	 * @param accident
	 *            The accident.
	 */
	void storeAccident(Accident accident);

	/**
	 * Loads a local authority specified by code.
	 * 
	 * @param code
	 *            The local authority code.
	 * @return Local authority specified by code.
	 */
	LocalAuthority loadLocalAuthorityByCode(String code);

	/**
	 * Loads an accident specified by id.
	 * 
	 * @param id
	 *            The accident id.
	 * @return Accident specified by id.
	 */
	Accident loadAccidentById(String id);

	/**
	 * Loads local authorities specified by codes.
	 * 
	 * @param codes
	 *            The local authority codes.
	 * @return Local authorities specified by codes.
	 */
	ArrayList<LocalAuthority> loadLocalAuthoritiesByCodes(Iterable<String> codes);

	/**
	 * Loads accidents specified by ids.
	 * 
	 * @param ids
	 *            The accident ids.
	 * @return Accidents specified by ids.
	 */
	ArrayList<Accident> loadAccidentsByIds(Iterable<String> ids);

	/**
	 * Finds local authority codes and the respective number of accidents in each
	 * ordered by the number of accidents.
	 * 
	 * @param numLocalAuthorityLimit
	 *            Limit of the number of local authorities to return.
	 * @return Local authority codes and the respective number of accidents in each.
	 */
	ArrayList<LocalAuthorityAccidentCount> loadLocalAuthorityAccidentCounts(int numLocalAuthorityLimit);

}
