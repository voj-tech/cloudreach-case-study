package net.voj_tech.cloudreach.accidents.data;

/**
 * Represents an accident.
 * 
 * @author voj-tech
 */
public class Accident {

	private String id;
	private String localAuthorityCode;

	public Accident() {
	}

	public Accident(String id, String localAuthorityCode) {
		this.id = id;
		this.localAuthorityCode = localAuthorityCode;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocalAuthorityCode() {
		return localAuthorityCode;
	}

	public void setLocalAuthorityCode(String localAuthorityCode) {
		this.localAuthorityCode = localAuthorityCode;
	}

}
