package net.voj_tech.cloudreach.accidents.data;

/**
 * Represents a pair of a local authority code and the respective number of
 * accidents.
 * 
 * @author voj-tech
 */
public class LocalAuthorityAccidentCount {

	private String localAuthorityCode;
	private long accidentCount;

	public LocalAuthorityAccidentCount(String localAuthorityCode, Long accidentCount) {
		this.localAuthorityCode = localAuthorityCode;
		this.accidentCount = accidentCount;
	}

	public String getLocalAuthorityCode() {
		return localAuthorityCode;
	}

	public Long getAccidentCount() {
		return accidentCount;
	}

}
